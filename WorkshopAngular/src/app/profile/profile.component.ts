import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { UserService } from '../form-input/service/user.service';
import { Location } from '@angular/common';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  constructor(    private formBuilder: FormBuilder,
    private location: Location,
    private userService:UserService) { }

  profile:any
  ngOnInit(): void {
    this.profile = this.userService.forminput;
    console.log(this.userService.forminput['fname'])
  }

  back(){

    this.location.back();
  }


}
