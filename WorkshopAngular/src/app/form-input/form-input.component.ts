import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { UserService } from './service/user.service';

@Component({
  selector: 'app-form-input',
  templateUrl: './form-input.component.html',
  styleUrls: ['./form-input.component.css']
})
export class FormInputComponent implements OnInit {

  formInput = this.formBuilder.group({
    fname: ['',Validators.required],
    lname: ['',Validators.required],
    age: [Validators.required,Validators.max(200)],
    Year:['',Validators.required],
    link:'',
    university:["",Validators.required]
  });

  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private userService:UserService,
    ) { }

  ngOnInit(): void {
    if(this.userService.forminput != null){
      this.formInput.setValue(
        {
          fname: this.userService.forminput['fname'],
          lname: this.userService.forminput['lname'],
          age: this.userService.forminput['age'],
          Year:this.userService.forminput['Year'],
          link: this.userService.forminput['link'],
          university:this.userService.forminput['university']
        }
      )
      
      
    }
  }

  link:any;
  test:any;
  onSubmit(){
    
    if(this.formInput.valid){
      this.test = this.formInput.value;
      this.userService.forminput =  this.formInput.value;
      Swal.fire({
        icon: 'success',
        title: 'Success',
        showConfirmButton: false,
        timer: 2000
      });
      setTimeout(() => {this.router.navigate(['Profile'])}, 2100);
    }else{
      Swal.fire({
        icon: 'error',
        title: 'Warning!',
        text: 'กรุณาป้อนข้อมูลให้ครบถ้วน',
        timer:2000
      })
    }
    
    

  }

  
}

