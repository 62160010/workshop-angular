import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormInputComponent } from './form-input/form-input.component';
import { ProfileComponent } from './profile/profile.component';

const routes: Routes = [
  {path:'Profile',component:ProfileComponent},
  {path:'',component:FormInputComponent},
  {path:'FormInput',component:FormInputComponent},
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
